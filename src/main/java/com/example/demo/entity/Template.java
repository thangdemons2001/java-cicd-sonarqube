package com.example.demo.entity;

public class Template {
	
	private long templateId;
	private String templateName;
	private String status;
	private String templateQuality;
	public long getTemplateId() {
		return templateId;
	}
	public void setTemplateId(long templateId) {
		this.templateId = templateId;
	}
	public String getTemplateName() {
		return templateName;
	}
	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getTemplateQuality() {
		return templateQuality;
	}
	public void setTemplateQuality(String templateQuality) {
		this.templateQuality = templateQuality;
	}
	
	public Template() {
		// TODO Auto-generated constructor stub
	}
	public Template(long templateId, String templateName, String status, String templateQuality) {
		super();
		this.templateId = templateId;
		this.templateName = templateName;
		this.status = status;
		this.templateQuality = templateQuality;
	}
	
	
}
