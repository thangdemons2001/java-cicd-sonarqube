package com.example.demo.entity;

import java.util.List;

public class Templates {
	
	private int error;
	
	private String message;
	
	private List<Template> data;
	
	private MetaData metadata;

	public int getError() {
		return error;
	}

	public void setError(int error) {
		this.error = error;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<Template> getData() {
		return data;
	}

	public void setData(List<Template> data) {
		this.data = data;
	}

	public MetaData getMetadata() {
		return metadata;
	}

	public void setMetadata(MetaData metadata) {
		this.metadata = metadata;
	}
	
	public Templates() {
		// TODO Auto-generated constructor stub
	}

	public Templates(int error, String message, List<Template> data, MetaData metadata) {
		super();
		this.error = error;
		this.message = message;
		this.data = data;
		this.metadata = metadata;
	}
	
	

}
